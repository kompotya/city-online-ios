//
//  Bids.swift
//  cityonline
//
//  Created by Michael Ovsienko on 20.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Bid{
    let bidId : String
    let bidDateInsert : String
    let bidStatus : String
    let bidStreetAddress : String
    let bidHouseAddress : String
    let bidFlatAddress  : String
    let bidCategory : String
    let bidSubCategory : String
    let bidMessage  : String
    let bidLatitude  : String
    let bidLongitude : String
    let bidPhotos : [String]
    
    required init(id : String, date : String, status : String, street : String, house :String, flat : String, category :String, subCategory : String, message : String, latitude : String, longitude : String, photos : [String] ) {
        self.bidId = id
        self.bidDateInsert = date
        self.bidStatus = status
        self.bidStreetAddress = street
        self.bidHouseAddress = house
        self.bidFlatAddress = flat
        self.bidCategory = category
        self.bidSubCategory = subCategory
        self.bidMessage = message
        self.bidLatitude = latitude
        self.bidLongitude = longitude
        self.bidPhotos = photos
    }
    
}
