//
//  BaseContract.swift
//  cityonline
//
//  Created by Michael Ovsienko on 20.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
protocol BaseContractView {
    func startLoader()
    func stopLoader()
}
