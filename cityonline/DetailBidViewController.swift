//
//  DetailBidViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 20.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMaps
import ImageViewer



class DetailBidViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout,GalleryItemsDatasource, GalleryDisplacedViewsDatasource {

    var bidDetail : Bid? = nil
    private var mUIImages = [UIImageView]()
    @IBOutlet weak var bidCategoryLabel: UILabel!
    @IBOutlet weak var bidAddressLabel: UILabel!
    @IBOutlet weak var bidDateLabel: UILabel!
    @IBOutlet weak var bidStatusLabel: UILabel!
    @IBOutlet weak var bidDescriptionLabel: UILabel!
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet weak var imagesCardView: CardView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var notateLabel: UILabel!
    
    @IBOutlet weak var markNameLabel: UILabel!
    @IBOutlet weak var statusNameLabel: UILabel!
    @IBOutlet weak var dateNameLabel: UILabel!
    @IBOutlet weak var basicInfo: UILabel!
    @IBOutlet weak var detailedDescriptionLabel: UILabel!
    @IBOutlet weak var imagesLabel: UILabel!
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var changeLog: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "bid_number".localized()+(bidDetail?.bidId)!
        
        self.bidCategoryLabel.text = (bidDetail?.bidCategory)! + "/" + (bidDetail?.bidSubCategory)!
        self.bidAddressLabel.text = (bidDetail?.bidStreetAddress)! + " " + (bidDetail?.bidHouseAddress)!
        if bidDetail?.bidFlatAddress != "" && bidDetail?.bidFlatAddress != "null"  {
            self.bidAddressLabel.text = self.bidAddressLabel.text! + "/" + (bidDetail?.bidFlatAddress)!
        }
        mUIImages = [UIImageView]()
        self.bidDateLabel.text  = bidDetail?.bidDateInsert
        self.bidStatusLabel.text = bidDetail?.bidStatus
        self.bidDescriptionLabel.text = bidDetail?.bidMessage
        self.moveCamera(lat: Double((bidDetail?.bidLatitude)!)!, lng: Double((bidDetail?.bidLongitude)!)!)
        if self.bidDetail?.bidPhotos.count == 0 {
            imagesCardView.goAway()
            var contentRect = CGRect.zero;
            imagesCardView.frame = CGRect.zero
            for view in self.scrollView.subviews {
                contentRect = contentRect.union(view.frame);
            }
            self.scrollView.contentSize = contentRect.size;
        }
        
        self.googleMapView.isMyLocationEnabled = true
        
        self.notateLabel.text = "Заявка додана у мобільному додатку"
        self.dateLabel.text = bidDetail?.bidDateInsert
        self.statusLabel.text = bidDetail?.bidStatus
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //CollectionView functions
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.bidDetail?.bidPhotos.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "images_bid", for: indexPath) as! ImageCollectionViewCell
        print(indexPath.row)
        
        cell.imageView.setShowActivityIndicator(true)
        cell.imageView.setIndicatorStyle(.gray)
        if self.bidDetail?.bidPhotos.count != 0{
            cell.imageView.sd_setImage(with: URL(string: (self.bidDetail?.bidPhotos[indexPath.row])!))
            mUIImages.append(cell.imageView)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showImagePreview(displacedIndex: indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = ((collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2) - 10
        let rightInset = leftInset
        return UIEdgeInsetsMake(0, CGFloat(leftInset), 0, rightInset)
    }

    
    
    internal func moveCamera (lat : Double, lng : Double){
        googleMapView.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat,
                                              longitude: lng,
                                              zoom: 11)
        let marker =    GMSMarker(position: CLLocationCoordinate2D.init(latitude: lat, longitude: lng))
        
        marker.map = googleMapView
        googleMapView.animate(to: camera)
        
    }
    
    
    
    
    //Image previewer functions
    func itemCount() -> Int {
        return mUIImages.count
    }
    
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        return UIImageView(image: mUIImages[index].image)
    }
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        
        let image = mUIImages[index].image
        return GalleryItem.image { $0(image) }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "bid_detail".localized()
        self.applyLocale()
    }
    
    
    func showImagePreview (displacedIndex : Int) {
        
        let frame = CGRect(x: 0, y: 0, width: 200, height: 24)
        let headerView = CounterView(frame: frame, currentIndex: displacedIndex, count: (self.bidDetail?.bidPhotos.count)!)
        let footerView = CounterView(frame: frame, currentIndex: displacedIndex, count: (self.bidDetail?.bidPhotos.count)!)
        let galleryViewController = GalleryViewController(startIndex: displacedIndex, itemsDatasource: self, displacedViewsDatasource: self, configuration: self.galleryConfiguration())
        galleryViewController.headerView = headerView
        galleryViewController.footerView = nil
        
        
        galleryViewController.landedPageAtIndexCompletion = { index in
            headerView.currentIndex = index
            footerView.currentIndex = index
        }
        
        self.presentImageGallery(galleryViewController)
        
    }
    func galleryConfiguration() -> GalleryConfiguration {
        return [
            
            GalleryConfigurationItem.pagingMode(.carousel),
            GalleryConfigurationItem.presentationStyle(.displacement),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            
            GalleryConfigurationItem.maximumZoolScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.linear),
            
            GalleryConfigurationItem.statusBarHidden(true),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(false),
            GalleryConfigurationItem.displacementInsetMargin(50)
        ]
    }
    
    func applyLocale(){
        self.basicInfo.text = "basic_info".localized()
        self.detailedDescriptionLabel.text = "detailed_description".localized()
        self.imagesLabel.text = "images".localized()
        self.mapLabel.text = "map".localized()
        self.changeLog.text = "change_log".localized()
        self.dateNameLabel.text = "date".localized()
        self.statusNameLabel.text = "status".localized()
        self.markNameLabel.text = "mark".localized()
    }
    
   

}
extension UIView {
    
    func goAway() {
        // set the width constraint to 0
        let widthConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
        superview!.addConstraint(widthConstraint)
        
        // set the height constraint to 0
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
        superview!.addConstraint(heightConstraint)
    }
    
}
