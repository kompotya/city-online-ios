//
//  User.swift
//  cityonline
//
//  Created by Michael Ovsienko on 21.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class User: NSObject, NSCoding {
    let cityID : Int
    let streetID : Int
    let userFlat : String
    let userHouse : String
    
    init (city : Int, street : Int, flat : String, house : String){
        self.cityID = city
        self.streetID = street
        self.userFlat = flat
        self.userHouse = house
    }
    required init(coder decoder : NSCoder) {
        self.cityID = Int(decoder.decodeCInt(forKey: "cityID"))
        self.streetID = Int(decoder.decodeCInt(forKey: "streetID"))
        self.userFlat = decoder.decodeObject(forKey: "userFlat") as! String
        self.userHouse = decoder.decodeObject(forKey: "userHouse") as! String
        

    }
    public func encode (with coder: NSCoder){
        coder.encode(cityID, forKey: "cityID")
        coder.encode(streetID, forKey: "streetID")
        coder.encode(userFlat, forKey: "userFlat")
        coder.encode(userHouse, forKey: "userHouse")
    }
}
