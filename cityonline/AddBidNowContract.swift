//
//  AddBidNowContract.swift
//  cityonline
//
//  Created by Michael Ovsienko on 07.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
import UIKit

protocol AddBidNowPresenterContract {
    init (view : AddBidNewView)
    func getCities (completion: @escaping(_ result: [City])->Void)
    func getStreets (index : Int, completion: @escaping(_ result: [Street])->Void)
    func getCategories(completion: @escaping(_ result: [Category])->Void )
    func getSubCategories (index : Int, completion: @escaping(_ result: [Category])->Void)
    func getLocationByAddress(address : String, completion: @escaping(_ result: [Double])->Void)
    func uploadPhotos (images : [UIImage], completion: @escaping (_ result: [String]) -> Void)
    func addBid (bidData  : AddBidAndReg, completion: @escaping (_ result: Bool)->Void)
    func addBidWithToken (bidData  : AddBidAndReg, token : String,completion: @escaping (_ result: Bool)->Void)
}
protocol AddBidNewView : BaseContractView {
    func moveCamera (lat : Double, lng : Double)
    func showDataPicker (textField : AnyObject, title : String, data : [String]) -> Bool
    func initializeViews ()
    func showImagePicker ()
    func showToast(message : String)
}
