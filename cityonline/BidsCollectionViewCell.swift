//
//  BidsCollectionViewCell.swift
//  cityonline
//
//  Created by Michael Ovsienko on 18.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class BidsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bidImage: UIImageView!
    @IBOutlet weak var bidNumber: UILabel!
    @IBOutlet weak var bidAddress: UILabel!
    @IBOutlet weak var bidCategory: UILabel!
    @IBOutlet weak var bidDate: UILabel!
    @IBOutlet weak var bidStatus: UILabel!
    
    
}
