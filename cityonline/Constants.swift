//
//  Constants.swift
//  cityonline
//
//  Created by Michael Ovsienko on 05.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Constants {
    public static let BASE_API_URL = "https://cityonline.com.ua"
    public static let GET_CITIES_API = "\(BASE_API_URL)/user/cities"
    public static let GET_STREETS_API = "\(BASE_API_URL)/user/streets/"
    public static let GET_CATEGORIES_API = "\(BASE_API_URL)/categories"
    public static let GET_SUBCATEGORIES_API = "\(BASE_API_URL)/subcategories/"
    public static let GET_LOCATION_API = "https://maps.googleapis.com/maps/api/geocode/json"
    public static let GET_USER_BIDS = "\(BASE_API_URL)/bids"
    public static let POST_REGISTRATION_API = "\(BASE_API_URL)/adduser"
    public static let POST_LOGIN_API = "\(BASE_API_URL)/login"
    public static let POST_UPLOAD_IMAGES = "\(BASE_API_URL)/file/upload"
    public static let POST_ADD_BID = "\(BASE_API_URL)/bid/add"
    public static let POST_FORGOT_PASSWORD = "\(BASE_API_URL)/forgot"
    public static let TOKEN = "token"
    public static let USER_DATA = "user_data"
}
