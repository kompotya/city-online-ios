//
//  RegData.swift
//  cityonline
//
//  Created by Michael Ovsienko on 05.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class RegData {
    private var userLogin : String
    private var userPassword : String
    private var confirmPassword : String
    private var userEmail : String
    private var userName : String
    private var userSurname : String
    private var userPatronymic : String
    private var userPhone : String
    private var userCityId : Int
    private var userStreetID : Int
    private var userHouse : String
    private var userFlat : String
    
    required init(login : String, password : String, confirmPass : String, email : String, name : String, surname : String, patronymic : String, phone : String, cityId : Int, streetId : Int, house : String, flat : String) {
        self.userLogin = login;
        self.userPassword = password;
        self.confirmPassword = confirmPass
        self.userEmail = email
        self.userName = name
        self.userSurname = surname
        self.userPatronymic = patronymic
        self.userPhone = phone
        self.userCityId = cityId
        self.userStreetID = streetId
        self.userHouse = house
        self.userFlat = flat
    }
    func getParameters () -> [String:Any] {
        let parameters : [String:Any] = [
            "userLogin": self.userLogin,
            "userPassword": self.userPassword,
            "rePass": self.confirmPassword,
            "userEmail": self.userEmail,
            "name": self.userName,
            "surname": self.userSurname,
            "otch": self.userPatronymic,
            "tel": self.userPhone,
            "cities_id": self.userCityId,
            "street": self.userStreetID,
            "house": self.userHouse,
            "flat": self.userFlat
        ]
        return parameters
    }
}
