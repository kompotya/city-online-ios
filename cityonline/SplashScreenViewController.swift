//
//  SplashScreenViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 19.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
               // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let token = UserDefaults.standard.string(forKey: Constants.TOKEN){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BidsNavigationController")
            self.present(vc!, animated: true, completion: nil)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController")
            self.present(vc!, animated: true, completion: nil)

        }
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
