//
//  Category.swift
//  cityonline
//
//  Created by Michael Ovsienko on 06.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Category {
    private let categoryId : Int
    private let CategoryName : String
    required init(id : Int, name : String) {
        self.categoryId = id
        self.CategoryName = name
    }
    required public init?(dictionary: NSDictionary) {
        self.categoryId = (dictionary["ID"] as? Int)!
        self.CategoryName = (dictionary["Name"] as? String)!
    }
    
    func getCategoryId () -> Int{
        return self.categoryId;
    }
    func getCategoryName() -> String {
        return self.CategoryName
    }
    public class func modelsFromDictionaryArray(array:NSArray) -> [Category]
    {
        var models:[Category] = []
        for item in array
        {
            models.append(Category(dictionary: item as! NSDictionary)!)
        }
        return models
    }

}
