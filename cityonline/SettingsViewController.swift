//
//  SettingsViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 20.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import DropDown
import Toaster
import Localize_Swift

class SettingsViewController: UIViewController {

    var pickerData: [String] = [String]()
    var languageData : [String] = [String]()
    
    let languageDropDown = DropDown()
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var chooseLanguageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerData = ["English", "Українська", "Русский"]
        languageData = ["en", "uk", "ru"]
        languageDropDown.anchorView = chooseLanguageButton
        
        languageDropDown.bottomOffset = CGPoint(x: 0, y: chooseLanguageButton.bounds.height)
        
        languageDropDown.dataSource = pickerData
        self.customizeDropDown(self)
        languageDropDown.selectionAction = { [unowned self] (index, item) in
           Localize.setCurrentLanguage(self.languageData[index])
            self.navigationItem.title = "Settings".localized()
            Toast(text: "language_changed_succesfull".localized(), duration: 2).show()
            self.applyLocale()

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.applyLocale()
    }
    
    @IBAction func chooseLanguage(_ sender: Any) {
        languageDropDown.show()
    }
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 0.9, alpha: 0.8)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.5)
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
    }
    func applyLocale (){
        self.languageLabel.text = "app_lang".localized()
        self.chooseLanguageButton.setTitle("choose_lang".localized(), for: .normal)
        self.navigationItem.title = "Settings".localized()
    }
    

}
