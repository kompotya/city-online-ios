//
//  ViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 21.03.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Localize_Swift
import Alamofire
import Toaster
import EGFormValidator

class ViewController: ValidatorViewController, UITextFieldDelegate {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var addBidButton: UIButton!    
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var errorPasswordLogin: UILabel!
    @IBOutlet weak var errorLoginLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    
    
    private var alert : UIAlertController? = nil
    
    @IBAction func addBidAction(_ sender: Any) {
        let addBidViewController = self.storyboard!.instantiateViewController(withIdentifier: "AddBidNowViewController") as! AddBidNowViewController      
        self.navigationController?.pushViewController(addBidViewController, animated: true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if self.validate() {
        loadIndicator.startAnimating()
        let parameters : [String : String] = [
            "userLogin": loginTextField.getValue() as! String,
            "userPassword" : passwordTextField.getValue() as! String
        ]
        Alamofire.request(Constants.POST_LOGIN_API, method: .post, parameters: parameters).responseJSON{
            response in
            switch (response.result){
            case .success:
                self.loadIndicator.stopAnimating()
                print(response.result.value)
                let result = response.result.value as! NSDictionary
                
                let statusCode = result["status_code"] as! Int
                               let message = Utils.checkStatusCode(statusCode: statusCode)
                Toast(text: message, duration: 2).show()

                if statusCode == 2002 {
                    let payload = result["payload"] as! NSDictionary
                    let token = payload["token"] as! String
                    let userData = payload["user"] as! NSDictionary
                    let flat : String
                    if let buffer = userData["flat"] as? String {
                     flat = buffer
                } else {
                    flat = ""
                }
                    let user = User(city: userData["cityID"] as! Int, street: userData["street"] as! Int , flat: flat, house: userData["house"] as! String)
                    
                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)                    
                    UserDefaults.standard.set(encodedData,forKey: Constants.USER_DATA)
                    UserDefaults.standard.set(token, forKey: Constants.TOKEN)
                    Singleton.sharedInstance.token = token
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BidsViewController") as! BidsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
            case .failure(let error):
                self.loadIndicator.stopAnimating()
                print(error)
                break
            }
        }
        }
    }
    @IBAction func registrationAction(_ sender: Any) {
        let registrationViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        
        self.navigationController?.pushViewController(registrationViewController, animated: true)
       }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        self.present(alert!, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.makeButtonRound(button: loginButton)
        self.makeButtonRound(button: registerButton)
        self.makeButtonRound(button: addBidButton)
        self.addValidators()
        alert = UIAlertController(title: "Forgot password?".localized(), message: "Enter a email".localized(), preferredStyle: .alert)
        
        alert?.addTextField { (textField) in
            textField.placeholder = "email_required".localized()
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert?.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            let parameters = [
                "userEmail" : textField?.getValue() as? String
            ]
            Alamofire.request(Constants.POST_FORGOT_PASSWORD, method: .post, parameters: parameters).responseJSON {
                response in
                switch response.result {
                case .success:
                    Toast(text: "data_sended".localized(), delay: 0, duration: 2).show()
                    break
                case .failure(let error):
                    Toast(text: error as! String, delay: 0, duration: 0).show()
                    break
                }
            }
        }))

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.applyLocaleString()
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addValidators(){
        self.addValidatorMandatory(toControl: loginTextField, errorPlaceholder: errorLoginLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: passwordTextField, errorPlaceholder: errorPasswordLogin, errorMessage: "required_field".localized())
        
        self.addValidatorMinLength(toControl: loginTextField, errorPlaceholder: errorLoginLabel, errorMessage: "min_length_6".localized(), minLength: 6)
        
        self.addValidatorMinLength(toControl: passwordTextField, errorPlaceholder: errorPasswordLogin, errorMessage: "min_length_6".localized(), minLength: 6)
    }
    
  

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func makeButtonRound (button : UIButton){
        button.layer.cornerRadius = 5
    }
    
    func applyLocaleString (){
        self.loginTextField.placeholder = "Login".localized()
        self.passwordTextField.placeholder = "Password".localized()
        self.loginButton.setTitle("Login".localized(), for: .normal)
        self.registerButton.setTitle("Register".localized(), for: .normal)
        self.addBidButton.setTitle("Add bid now".localized(), for: .normal)
        self.forgotPasswordButton.setTitle("Forgot password?".localized(), for: .normal)
    }
    
    
}

