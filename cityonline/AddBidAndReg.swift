//
//  AddBidAndReg.swift
//  cityonline
//
//  Created by Michael Ovsienko on 11.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class AddBidAndReg {
    private let bidImages : [String]
    private let bidStreet : Int
    private let bidHouse : String
    private let bidFlat : String
    private let bidMessage : String
    private let bidCategoriesID : Int
    private let bidSubcategoriesID : Int
    private let bidLatitude : Double
    private let bidLongitude : Double
    private let bidOnUserAddress : Bool
    private let bidChangedManually : Int
    private let bidCityID : Int
    private let userEmail : String
    private let userName : String
    private let userSurname : String
    private let userPatronymic : String
    private let userStreet : Int
    private let userHouse : String
    private let userFlat : String
    private let userPhone : String
    private let isRegister : Bool
    
    init(bidImages : [String], bidStreet : Int, bidHouse : String, bidMessage : String, bidFlat : String, bidCategoriesID : Int, bidSubcategoriesID : Int, bitLatitude : Double, bidLongitude : Double, bidOnUserAddres : Bool, bidChangedManually : Int, bidCityID : Int, userEmail : String,userName : String, userSurname : String, userPatronymic : String, userStreet : Int, userHouse : String, userFlat : String, userPhone : String, isRegister : Bool ) {
            self.bidImages = bidImages
            self.bidStreet = bidStreet
            self.bidHouse = bidHouse
            self.bidFlat = bidFlat
            self.bidMessage = bidMessage
            self.bidCategoriesID = bidCategoriesID
            self.bidSubcategoriesID = bidSubcategoriesID
            self.bidLatitude = bitLatitude
            self.bidLongitude = bidLongitude
            self.bidOnUserAddress = bidOnUserAddres
            self.bidChangedManually = bidChangedManually
            self.bidCityID = bidCityID
            self.userEmail = userEmail
            self.userName = userName
            self.userSurname = userSurname
            self.userPatronymic = userPatronymic
            self.userStreet = userStreet
            self.userHouse = userHouse
            self.userFlat = userFlat
            self.userPhone = userPhone
            self.isRegister = isRegister
    }
    
    func getParameters() ->  [String:Any]{
        return [
            "bidImages" : self.bidImages,
            "bid_street" : self.bidStreet,
            "bid_house" : self.bidHouse,
            "bid_flat" : self.bidFlat,
            "bidMessage" : self.bidMessage,
            "categories" : self.bidCategoriesID,
            "subCategories" : self.bidSubcategoriesID,
            "lat" : self.bidLatitude,
            "lng" : self.bidLongitude,
            "isBidOnUserAddress" : self.bidOnUserAddress,
            "changed_manually" : self.bidChangedManually,
            "city" : self.bidCityID,
            "email" : self.userEmail,
            "register"  : self.isRegister,
            "surname" : self.userSurname,
            "name" : self.userName,
            "otch" : self.userPatronymic,
            "userStreet" : self.userStreet,
            "userHouse" : self.userHouse,
            "userFlat" : self.userFlat,
            "tel" : self.userPhone
        ]
    }
    func getParametersWithToken() ->  [String:Any]{
        return [
            "bidImages" : self.bidImages,
            "bid_street" : self.bidStreet,
            "bid_house" : self.bidHouse,
            "bid_flat" : self.bidFlat,
            "bidMessage" : self.bidMessage,
            "categories" : self.bidCategoriesID,
            "subCategories" : self.bidSubcategoriesID,
            "lat" : self.bidLatitude,
            "lng" : self.bidLongitude,
            "isBidOnUserAddress" : self.bidOnUserAddress,
            "changed_manually" : self.bidChangedManually
        ]
    }
}
