//
//  Street.swift
//  cityonline
//
//  Created by Michael Ovsienko on 05.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Street {
    private let streetId : Int
    private let streetName : String
    required init(id : Int, name : String) {
        self.streetId = id
        self.streetName = name
    }
    required public init?(dictionary: NSDictionary) {
        self.streetId = (dictionary["ID"] as? Int)!
        self.streetName = (dictionary["Name"] as? String)!
    }
    
    func getStreetID () -> Int{
        return self.streetId;
    }
    func getStreetName() -> String {
        return self.streetName
    }
    public class func modelsFromDictionaryArray(array:NSArray) -> [Street]
    {
        var models:[Street] = []
        for item in array
        {
            models.append(Street(dictionary: item as! NSDictionary)!)
        }
        return models
    }
}
