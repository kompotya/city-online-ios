//
//  ImagesCollectionViewCell.swift
//  cityonline
//
//  Created by Michael Ovsienko on 07.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
