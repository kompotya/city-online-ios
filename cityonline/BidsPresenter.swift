//
//  BidsPresenter.swift
//  cityonline
//
//  Created by Michael Ovsienko on 18.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
import Alamofire
class BidsPresenter : BidsPresenterContract {
    private var view : BidsView? = nil
    required init(view : BidsView) {
        self.view = view
    }
    
    func getUsersBids(token: String, completion: @escaping ([Bid]) -> Void) {
        let header : [String:String] = [
            "Authorization" : token
        ]
        view?.startLoader()
        Alamofire.request(Constants.GET_USER_BIDS, method: .get, headers: header).responseJSON {
            response in
            switch response.result{
            case .success:
                self.view?.stopLoader()
                let jsonResponse = response.result.value as? NSDictionary
                let bids = jsonResponse?["bids"] as! NSArray
                var bidsArray = [Bid]()

                for i in 0..<bids.count {
                    let bid = bids[i] as! NSDictionary
                    let number = bid["ID"] as! String
                    let date = bid["DateInsert"] as! String
                    let status = bid["Status"] as! String
                    let street = bid["Street"] as! String
                    let house = bid["Building"] as! String
                    let flat = bid["Flat"] as! String
                    let category = bid["Category"] as! String
                    let subCategory = bid["SubCategory"] as! String
                    let message = bid["MessageText"] as! String
                    let photos : [String]
                    if let buffer = bid["photo"] as? [String]  {
                       photos = buffer
                    } else {
                         photos = [String]()
                    }
                        
                    let latitude = bid["Latitude"] as! String
                    let longitude = bid["Longitude"] as! String
                    
                  
                    
                    bidsArray.append(Bid(id: number, date: self.getDateString(date: date), status: status, street: street, house: house, flat: flat, category: category, subCategory: subCategory, message: message, latitude: latitude, longitude: longitude, photos: photos))
                }
                completion(bidsArray)
                break
            case .failure(let error):
                print(error)
                self.view?.stopLoader()
                break
            }
        }
    }
    func getDateString (date : String) -> String {
        let dateFormatter = DateFormatter()
        
        let createdDate = dateFormatter.date(fromSwapiString: date)
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        let dateString = dateFormatter.string(from: createdDate!)
        
        return dateString
    }
    
}
extension DateFormatter {
    func date(fromSwapiString dateString: String) -> Date? {
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.locale = Locale(identifier: "ru_UA")
        return self.date(from: dateString)
    }
}
