//
//  BidsContract.swift
//  cityonline
//
//  Created by Michael Ovsienko on 18.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
protocol BidsPresenterContract {
    func getUsersBids (token : String, completion: @escaping (_ result: [Bid])->Void )
}
protocol BidsView : BaseContractView {
    func reloadData()
}
