//
//  Utils.swift
//  cityonline
//
//  Created by Michael Ovsienko on 05.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Utils {
    public class func checkStatusCode (statusCode : Int) -> String {
        let message : String
        switch statusCode {
        case 2002:
            message = "Authorization successful"
            break
        case 4008:
            message = "Registration successful. The registration data was sent by e-mail"
            break
        case 4007:
            message = "Error filling required fields"
            break
        case 4001:
            message = "Error entering email address"
            break
        case 4002:
            message = "Wrong login or password"
            break
        case 4011:
            message = "Login not found"
            break
        case 4006:
            message = "Error - password and password repeat do not match"
            break
        case 4005:
            message = "Error - this user already exists"
            break
        case 4021:
            message = "Error - user with this email already exists"
            break
        case 4031:
            message = "Error - Place's coordinates aren't in target city"
            break
        case 4010:
            message = "Error filling the category, subcategory and / or description of the application"
            break
        case 5005:
            message = "Error filling the address of the application"
            break
        case 4022:
            message = "Error filling the city identifier and e-mail address (only for unauthorized users)"
            break
        case 4023:
            message = "An error occurred while trying to create an application by an unauthorized user at the user's address (only for unauthorized users)"
            break
        case 4001:
            message = "The indicated coordinates extend beyond the boundaries of the indicated city)"
            break
        case 5004:
            message = "Error adding claim"
            break
        case 5000:
            message = "Internal server error"
            break
        case 5012:
            message = "There are no city coordinates (server side)"
            break
        default:
            message = "Internal server error"
        }
        return message
    }
}
