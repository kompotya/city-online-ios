//
//  RegistrationViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 04.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import EGFormValidator
import ActionSheetPicker_3_0
import Alamofire
import Toaster
import NVActivityIndicatorView


class RegistrationViewController: ValidatorViewController, UITextFieldDelegate,NVActivityIndicatorViewable {

    
    
    @IBOutlet weak var requiredFields: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var errorLoginLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var errorEmailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var surnameTextField: UITextField!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    
    
    @IBOutlet weak var patronymicLabel: UILabel!
    @IBOutlet weak var patronymicTextField: UITextField!
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var errorPasswordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var errorConfirmPasswordLabel: UILabel!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var errorCityLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var errorStreetLabel: UILabel!
    @IBOutlet weak var streetTextField: UITextField!
    
    
    @IBOutlet weak var houseLabel: UILabel!
    @IBOutlet weak var errorHouseLabel: UILabel!
    @IBOutlet weak var houseTextField: UITextField!
    
    @IBOutlet weak var apartmentLabel: UILabel!
    @IBOutlet weak var apartmentTextField: UITextField!
    
    @IBOutlet weak var registrationButton: UIButton!
    
    @IBOutlet weak var grayView: UIView!
    
    private var cities = [City]()
    private var streets = [Street]()
    private var citiesName = [String]()
    private var streetsName = [String]()
    
    private var citiesPickerIndex : Int = 0
    private var streetsPickerIndex : Int = 0
    private var activityIndicatorView : NVActivityIndicatorView? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyLocale()
        let wigth = self.view.frame.width * 0.2
        let heigth = self.view.frame.height * 0.2
        let frame = CGRect(x: self.view.center.x - wigth/2.0 , y: self.view.center.y - heigth/2.0, width: wigth, height: heigth)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: .ballRotateChase, color: UIColor(red: 9/255.0, green: 148/255.0, blue: 122/255.0, alpha: 1))
        self.view.addSubview(activityIndicatorView!)
        
        registrationButton.layer.cornerRadius = 5

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.hideKeyboard))
        view.addGestureRecognizer(tap)
        cityTextField.tag = 9
        streetTextField.tag = 10        
      
        self.addValidators()
        self.getCities()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.applyLocale()
    }
    
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(textField.tag)
        switch textField.tag {
        case 9:
            self.view.endEditing(true)
            return  showPicker(textField: textField, title: "Choose city".localized(), data: self.citiesName);
        case 10:
            self.view.endEditing(true)
            return showPicker(textField: textField, title: "Choose street".localized(), data: self.streetsName)
        default:
            return true
        }
    }
    func hideKeyboard () {
        self.view.endEditing(true)
    }
    
    func showPicker(textField : UITextField, title : String, data : [String]) -> Bool {
        let actionPicker =  ActionSheetStringPicker(title: title, rows: data, initialSelection: 1, doneBlock: {
            picker, indexes, values in
            textField.text = values as? String
            if textField.tag == 9 {
                self.citiesPickerIndex = indexes
                self.getStreets(index: indexes)
            } else {
                self.streetsPickerIndex = indexes
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: textField)
        
        actionPicker?.setCancelButton(UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action:nil))
        actionPicker?.setDoneButton(UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action:nil))
        actionPicker?.show()
        return false
    }
    func addValidators (){
        self.addValidatorMinLength(toControl: self.loginTextField,
                                   errorPlaceholder: self.errorLoginLabel,
                                   errorMessage: "Minimal length is 6",
                                   minLength: 6)
        self.addValidatorEmail(toControl: self.emailTextField,
                               errorPlaceholder: self.errorEmailLabel,
                               errorMessage: "Invalid email"
                              )
        self.addValidatorMinLength(toControl: self.passwordTextField,
                                   errorPlaceholder: self.errorPasswordLabel,
                                   errorMessage: "Minimal length is 6",
                                   minLength: 6)
        self.addValidatorEqualTo(toControl: self.confirmPasswordTextField,
                                 errorPlaceholder: self.errorConfirmPasswordLabel,
                                 errorMessage: "Passwords doesnt match",
                                 compareWithControl: self.passwordTextField)
        
        
        
        self.addValidatorMandatory(toControl: self.loginTextField,
                                   errorPlaceholder: self.errorLoginLabel,
                                   errorMessage: "Required field")
        
        
        self.addValidatorMandatory(toControl: self.emailTextField,
                                   errorPlaceholder: self.errorEmailLabel,
                                   errorMessage: "Required field")
        
        
        self.addValidatorMandatory(toControl: self.passwordTextField,
                                   errorPlaceholder: self.errorPasswordLabel,
                                   errorMessage: "Required field")
        
        
        self.addValidatorMandatory(toControl: self.houseTextField,
                                   errorPlaceholder: self.errorHouseLabel,
                                   errorMessage: "Required field")
        


        
        self.addValidatorMandatory(toControl: self.cityTextField,
                                   errorPlaceholder: self.errorCityLabel,
                                   errorMessage: "Must be choosed")
        self.addValidatorMandatory(toControl: self.streetTextField,
                                   errorPlaceholder: self.errorStreetLabel,
                                   errorMessage: "Must be choosed"){ [unowned self] () -> Bool in
                                    if let city = self.cityTextField.getValue() as? String, city.characters.count > 0 {
                                        return false
                                    }
                                    return true
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.layer.position.y > self.view.center.y {
            scrollView.setContentOffset(CGPoint(x: 0,y: 700), animated: true)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 400), animated: true)
       }
    
    @IBAction func registrationAction(_ sender: Any) {
        
        if self.validate() {
            // show success alert
            let regData : RegData = RegData(login: self.loginTextField.getValue() as! String,
                                            password: self.passwordTextField.getValue() as! String,
                                            confirmPass: self.confirmPasswordTextField.getValue() as! String,
                                            email: self.emailTextField.getValue() as! String,
                                            name: self.nameTextField.getValue() as! String,
                                            surname: self.surnameTextField.getValue() as! String,
                                            patronymic: self.patronymicTextField.getValue() as! String,
                                            phone: self.phoneTextField.getValue() as! String,
                                            cityId: cities[citiesPickerIndex].getCityId() ,
                                            streetId: streets[streetsPickerIndex].getStreetID() ,
                                            house: self.houseTextField.getValue() as! String,
                                            flat: self.apartmentTextField.getValue() as! String)
                    Alamofire.request(Constants.POST_REGISTRATION_API, method: .post, parameters: regData.getParameters(), encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
            
                        switch response.result {
                        case .success:
                            let result = response.result.value as! NSDictionary
                            let statusCode = result["status_code"] as! Int
                            let message = Utils.checkStatusCode(statusCode: statusCode)
                            Toast(text: message, duration: 2).show()

                            if  statusCode == 4008 {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            break
                        case .failure(let error):
                            print(error)
                            break
                        }
                    }
            
        } else {
            self.scrollView?.scrollRectToVisible(self.firstFailedControl!.frame, animated: true)
        }
    }
    
    func getCities()  {
        self.startLoader()
        print(Constants.GET_CITIES_API)
        Alamofire.request(Constants.GET_CITIES_API, method: .get, parameters: nil).responseJSON{
            response in
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                self.cities = City.modelsFromDictionaryArray(array: jsonResponse?["cities"] as! NSArray)
                for city in self.cities {
                    self.citiesName.append(city.getCityName())
                }
                self.stopLoader()
                break
                
            case .failure(let error):
                print(error)
                self.stopLoader()
                break
            }
            
        }
        
    }
    func getStreets(index : Int) {
        self.startLoader()

        self.streetsName = [String]()
        print(Constants.GET_STREETS_API+String(cities[index].getCityId()))
        Alamofire.request(Constants.GET_STREETS_API+String(cities[index].getCityId()), method: .get, parameters: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    let jsonResponse  = response.result.value as?  NSDictionary
                    
                    guard let streetsNSArray = jsonResponse?["streets"] as? NSArray,
                        let bufStreets = Street.modelsFromDictionaryArray(array: streetsNSArray) as? [Street] else {
                        return
                    }
                    self.streets = bufStreets
                    for street in self.streets{
                        self.streetsName.append(street.getStreetName())
                    }
                    self.stopLoader()
                    break
                case .failure(let error):
                    self.stopLoader()
                    print(error)
                    break
            }

        }

    }
    
    func startLoader() {
        self.scrollView.isUserInteractionEnabled = false
        grayView.isHidden = false;
        activityIndicatorView?.startAnimating()
        
    }
    func stopLoader() {
        grayView.isHidden = true;
        self.scrollView.isUserInteractionEnabled = true
        activityIndicatorView?.stopAnimating()
        
    }
    
    func applyLocale() {
        self.requiredFields.text = "required".localized()
        self.loginLabel.text = "Login".localized()
        self.emailLabel.text = "email_required".localized()
        self.surnameLabel.text = "surname".localized()
        self.nameLabel.text = "name".localized()
        self.patronymicLabel.text = "patronymic".localized()
        self.passwordLabel.text = "password_required".localized()
        self.confirmPasswordLabel.text = "confirm_required".localized()
        self.phoneLabel.text = "phone".localized()
        self.cityLabel.text = "city_required".localized()
        self.streetLabel.text = "street_required".localized()
        self.houseLabel.text = "house_required".localized()
        self.apartmentLabel.text = "apartment".localized()
        self.registrationButton.setTitle("Register".localized(), for: .normal)
    }
}
