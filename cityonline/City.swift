//
//  City.swift
//  cityonline
//
//  Created by Michael Ovsienko on 05.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class City {
    private let cityId : Int
    private let nameCity: String
    required init(id : Int, name : String) {
        self.cityId = id
        self.nameCity = name
    }
    required public init?(dictionary: NSDictionary) {
        self.cityId = (dictionary["city_id"] as? Int)!
        self.nameCity = (dictionary["name_ru"] as? String)!
    }
    
    func getCityId() -> Int {
        return self.cityId
    }
    func getCityName() -> String {
        return self.nameCity
    }
    public class func modelsFromDictionaryArray(array:NSArray) -> [City]
    {
        var models:[City] = []
        for item in array
        {
            models.append(City(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
}
