//
//  AddNewBidViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 20.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import M13Checkbox
import CoreLocation
import ActionSheetPicker_3_0
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import ImagePicker
import KCFloatingActionButton
import ImageViewer
import EGFormValidator
import Photos
import Toaster

class AddNewBidViewController: ValidatorViewController, AddBidNewView, UITextFieldDelegate,UITextViewDelegate, ImagePickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, GalleryItemsDatasource, GalleryDisplacedViewsDatasource {

    private var mStreets = [Street]()
    private var mCategories = [Category]()
    private var mSubCategories = [Category]()
    private var mStreetsName = [String]()
    private var mCategoriesName = [String]()
    private var mSubCategoriesName = [String]()
    private var mUploadFileNames = [String]()
    private var mUIImages = [UIImage]()
    private var mCitiesPickerIndex : Int = 0
    private var mStreetsPickerIndex : Int = 0
    private var mCategoriesPickerIndex : Int = 0
    private var mSubCategoriesPickerIndex : Int = 0
    private var mPresenter : AddBidNowPresenter? = nil
    private var mCityID : Int = 0
    private var mStreetID : Int = 0
    private var mCityName : String = ""
    private var mStreetName : String = ""
    private var latitude  : Double = 0.0
    private var longitude : Double = 0.0
    private var firstServiceCallComplete = false
    private var secondServiceCallComplete = false
    private var thirdServiceCallComplete = false


    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var checkBoxUseMyData: M13Checkbox!
    @IBOutlet weak var errorStreetLabel: UILabel!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var errorHouseLabel: UILabel!
    @IBOutlet weak var houseTextField: UITextField!
    @IBOutlet weak var appartmentTextField: UITextField!
    @IBOutlet weak var errorCategoryLabel: UILabel!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var subCategoryLabel: UILabel!
    @IBOutlet weak var subCategoryTextField: UITextField!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var grayView: UIView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    
    @IBOutlet weak var requiredLabel: UILabel!
    
    @IBOutlet weak var useMyRegistrationLabel: UILabel!
    @IBOutlet weak var houseLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var apartmentLabel: UILabel!
    @IBOutlet weak var categoryRequiredLabel: UILabel!
    @IBOutlet weak var subCategoryRequiredLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var useMyLocationLabel: UILabel!
    
    //MARK: - views
    private var activityIndicatorView : NVActivityIndicatorView? = nil
    private var fab = KCFloatingActionButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeViews()
        self.addValidators()

        mCityID = (Singleton.sharedInstance.user?.cityID)!
        mStreetID  = (Singleton.sharedInstance.user?.streetID)!
        mPresenter = AddBidNowPresenter(view: self)
        self.getInitialData()
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        imagesCollectionView.isHidden = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getInitialData () {
        mPresenter?.getCities(){ (result) -> Void in
            for city in result {
                if city.getCityId() == self.mCityID {
                    self.mCityName = city.getCityName()
                    break
                }
            }
            self.firstServiceCallComplete = true
            self.handleServiceCallCompletion()
        }
        mPresenter?.getCategories(){ (result)->Void in
            self.mCategories = result
            self.secondServiceCallComplete = true
            self.handleServiceCallCompletion()
        }
        mPresenter?.getStreetsByCityID(cityID: mCityID){
            result in
            for street in result {
                if street.getStreetID() == self.mStreetID {
                    self.mStreetName = street.getStreetName()
                    break
                }
            }
            self.mStreets = result
            self.thirdServiceCallComplete = true
            self.handleServiceCallCompletion()
        }
    }
    
    func initializeViews() {
        fab.addItem("Add photos".localized(), icon: UIImage(named: "add_photo")) { item in
            self.showImagePicker()
        }
        fab.addItem("Add bid".localized(), icon: UIImage(named: "add_bid")) { item in
            if self.validate() {
                self.mPresenter?.uploadPhotos(images: self.mUIImages){
                    (result) -> Void in
                    
                    self.mUploadFileNames = result
                    
                    let bidData = AddBidAndReg(bidImages: self.mUploadFileNames,
                                               bidStreet: self.mStreets[self.mStreetsPickerIndex].getStreetID(),
                                               bidHouse: self.houseTextField.getValue() as! String,
                                               bidMessage: self.descriptionTextView.getValue() as! String,
                                               bidFlat: self.appartmentTextField.getValue() as! String,
                                               bidCategoriesID: self.mCategories[self.mCategoriesPickerIndex].getCategoryId(),
                                               bidSubcategoriesID: self.mSubCategories[self.mSubCategoriesPickerIndex].getCategoryId(),
                                               bitLatitude: self.latitude,
                                               bidLongitude: self.longitude,
                                               bidOnUserAddres: true,
                                               bidChangedManually: 1,
                                               bidCityID: self.mCityID,
                                               userEmail: "",
                                               userName: "",
                                               userSurname: "",
                                               userPatronymic: "",
                                               userStreet: 123,
                                               userHouse: "",
                                               userFlat: "",
                                               userPhone: "",
                                               isRegister: true)
                    
                    self.mPresenter?.addBidWithToken(bidData: bidData, token: Singleton.sharedInstance.token){ (result) -> Void in
                        let isShowing = result as! Bool
                        if isShowing {
                            Toast(text: "bid_added".localized(), duration: 2.0).show()
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BidsNavigationController")
                            self.present(vc!, animated: true, completion: nil)
                    }
                }
                    
                }
                
                
                
                //TODO: presenter make post request
            } else {
                self.scrollView.scrollRectToVisible((self.firstFailedControl?.frame)!, animated: true)
            }
        }
        self.view.addSubview(fab)
        //Others views
        descriptionTextView.delegate = self
        streetTextField.tag = 10
        categoryTextField.tag = 11
        subCategoryTextField.tag = 12
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.layer.borderColor = UIColor.gray.cgColor
        let wigth = self.view.frame.width * 0.2
        let heigth = self.view.frame.height * 0.2
        let frame = CGRect(x: self.view.center.x - wigth/2.0 , y: self.view.center.y - heigth/2.0, width: wigth, height: heigth)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: .ballRotateChase, color: UIColor(red: 9/255.0, green: 148/255.0, blue: 122/255.0, alpha: 1))
        self.view.addSubview(activityIndicatorView!)
    }
    //Delegate functions
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 10:
            self.view.endEditing(true)
            return showDataPicker(textField: textField, title: "Choose street".localized(), data: self.mStreetsName)
        case 11:
            self.view.endEditing(true)
            return showDataPicker(textField: textField, title: "Choose category".localized(), data: self.mCategoriesName)
        case 12:
            self.view.endEditing(true)
            return showDataPicker(textField: textField, title: "Choose subcategory".localized(), data: self.mSubCategoriesName)
        default:
            return true
        }
    }

    //Show elements functions
    internal func showImagePicker() {
        let imagePickerController = ImagePickerController()
        var config = Configuration()
        config.doneButtonTitle = "Finish".localized()
        config.recordLocation = false
        config.noImagesTitle = "Sorry! There are no images here!".localized()
        imagePickerController.delegate = self
        imagePickerController.configuration = config
        imagePickerController.imageLimit = 3 - mUIImages.count
        present(imagePickerController, animated: true, completion: nil)
    }
    internal func showDataPicker(textField: AnyObject, title: String, data: [String]) -> Bool{
        let textField = textField as! UITextField
        let actionPicker =  ActionSheetStringPicker(title: title, rows: data, initialSelection: 1, doneBlock: {
            picker, indexes, values in
            textField.text = values as? String
            switch textField.tag {
            case 10:
                self.mStreetsPickerIndex = indexes
                self.mPresenter?.getLocationByAddress(address: self.mCityName + (self.streetTextField.getValue() as! String)){
                    result in
                    self.latitude = result[0]
                    self.longitude = result[1]
                }
                break
            case 11:
                self.mCategoriesPickerIndex = indexes
                self.getSubCategories(index: indexes)
                break
            case 12:
                self.mSubCategoriesPickerIndex = indexes
                break
            default:
                break
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: textField)
        actionPicker?.setCancelButton(UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action:nil))
        actionPicker?.setDoneButton(UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action:nil))
        actionPicker?.show()
        return false
    }
    func showImagePreview (displacedIndex : Int) {
        
        let frame = CGRect(x: 0, y: 0, width: 200, height: 24)
        let headerView = CounterView(frame: frame, currentIndex: displacedIndex, count: mUIImages.count)
        let footerView = CounterView(frame: frame, currentIndex: displacedIndex, count: mUIImages.count)
        let galleryViewController = GalleryViewController(startIndex: displacedIndex, itemsDatasource: self, displacedViewsDatasource: self, configuration: self.galleryConfiguration())
        galleryViewController.headerView = headerView
        galleryViewController.footerView = nil
        
        
        galleryViewController.landedPageAtIndexCompletion = { index in
            headerView.currentIndex = index
            footerView.currentIndex = index
        }
        
        self.presentImageGallery(galleryViewController)
        
    }
    
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        print("didPress")
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count != 0 {
            imagesCollectionView.isHidden = false
            
            imagePicker.dismiss(animated: true, completion: nil)
            for imageAsset in imagePicker.stack.assets {
                let options = PHContentEditingInputRequestOptions()
                options.isNetworkAccessAllowed = true
                imageAsset.requestContentEditingInput(with: options) { (contentEditingInput: PHContentEditingInput?, _) -> Void in
                    let url = contentEditingInput!.fullSizeImageURL!
                    self.mUploadFileNames.append("\(url)")
                    
                }
            }
            
            for image in images {
                mUIImages.append(image)
            }
            self.imagesCollectionView.reloadData()
        }
        
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    internal func startLoader() {
        if !(activityIndicatorView?.isAnimating)! {
            self.scrollView.isUserInteractionEnabled = false
            grayView.isHidden = false;
            activityIndicatorView?.startAnimating()
        }
        
    }
    internal func stopLoader() {
        if (activityIndicatorView?.isAnimating)!{
            grayView.isHidden = true;
            self.scrollView.isUserInteractionEnabled = true
            activityIndicatorView?.stopAnimating()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.applyLocale()
    }
    
    //CollectionView functions
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mUIImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_view_cell_images", for: indexPath) as! ImagesCollectionViewCell
        cell.imageView.image = mUIImages[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        let leftInset : CGFloat
        let rightInset : CGFloat
        let collectionViewHeight = collectionView.bounds.height
        let collectionViewWidth = collectionView.bounds.width
        let numberOfItemsThatCanInCollectionView = Int(collectionViewWidth / collectionViewHeight)
        if numberOfItemsThatCanInCollectionView > mUIImages.count {
            let totalCellWidth = collectionViewHeight * CGFloat(mUIImages.count)
            let totalSpacingWidth: CGFloat = CGFloat(mUIImages.count) * (CGFloat(mUIImages.count) - 1)
            // leftInset, rightInset are the global variables which I am passing to the below function
            leftInset = (collectionViewWidth - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
            rightInset = -leftInset
        } else {
            leftInset = 0.0
            rightInset = -collectionViewHeight
        }
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showImagePreview(displacedIndex: indexPath.row)
    }
    
    
    //Image previewer functions
    func itemCount() -> Int {
        return mUIImages.count
    }
    
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        return UIImageView(image: mUIImages[index])
    }
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let image = mUIImages[index]
        return GalleryItem.image { $0(image) }
        
    }
    
    func getSubCategories (index : Int){
        self.mSubCategoriesName = [String]()
        mPresenter?.getSubCategories(index: index){ (result) -> Void in
            self.mSubCategories = result
            for subcategory in self.mSubCategories {
                self.mSubCategoriesName.append(subcategory.getCategoryName())
            }
        }
        
        
        
    }
    func addValidators (){
        self.addValidatorMandatory(toControl: streetTextField, errorPlaceholder: errorStreetLabel, errorMessage: "Required field".localized())
        self.addValidatorMandatory(toControl: houseTextField, errorPlaceholder: errorHouseLabel, errorMessage: "Required field".localized())
        self.addValidatorMandatory(toControl: categoryTextField, errorPlaceholder: errorCategoryLabel, errorMessage: "Required field".localized())
        self.addValidatorMandatory(toControl: subCategoryTextField, errorPlaceholder: subCategoryLabel, errorMessage: "Required field".localized())
        self.addValidatorMandatory(toControl: descriptionTextView, errorPlaceholder: errorDescriptionLabel, errorMessage: "Required field".localized())
        self.addValidatorMinLength(toControl: descriptionTextView, errorPlaceholder: errorDescriptionLabel, errorMessage: "Minimal length is 50".localized(), minLength: 10)
    }
    func getStreets(index : Int) {
        self.mStreetsName = [String]()
        mPresenter?.getStreets(index: index){ (result) -> Void in
            self.mStreets = result
            for  street in self.mStreets {
                self.mStreetsName.append(street.getStreetName())
            }
        }
        
    }
    
    func handleServiceCallCompletion() {
        if firstServiceCallComplete && secondServiceCallComplete && thirdServiceCallComplete {
            self.stopLoader()
            for category in self.mCategories {
                mCategoriesName.append(category.getCategoryName())
            }
            for street in self.mStreets {
                mStreetsName.append(street.getStreetName())
            }
            
        }
        
    }
    
    func showToast(message: String) {
        Toast(text: message, duration: 2).show()
    }
    
    func galleryConfiguration() -> GalleryConfiguration {
        return [
            
            GalleryConfigurationItem.pagingMode(.carousel),
            GalleryConfigurationItem.presentationStyle(.displacement),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            
            GalleryConfigurationItem.maximumZoolScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.linear),
            
            GalleryConfigurationItem.statusBarHidden(true),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(false),
            GalleryConfigurationItem.displacementInsetMargin(50)
        ]
    }
    internal func moveCamera (lat : Double, lng : Double){
        googleMap.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat,
                                              longitude: lng,
                                              zoom: 13)
        let marker =    GMSMarker(position: CLLocationCoordinate2D.init(latitude: lat, longitude: lng))
        
        marker.map = googleMap
        googleMap.animate(to: camera)
        
    }
    
    @IBAction func onCheckedChanged(_ sender: Any) {
        if checkBoxUseMyData._IBCheckState == "Checked" {
            self.streetTextField.isUserInteractionEnabled = false
            self.houseTextField.isUserInteractionEnabled = false
            self.appartmentTextField.isUserInteractionEnabled = false
            self.streetTextField.text = mStreetName
            self.houseTextField.text = Singleton.sharedInstance.user?.userHouse
            if Singleton.sharedInstance.user?.userFlat != "" && Singleton.sharedInstance.user?.userFlat != "null" {
                self.appartmentTextField.text = Singleton.sharedInstance.user?.userFlat
            }
            self.mPresenter?.getLocationByAddress(address: self.mCityName + self.mStreetName + (Singleton.sharedInstance.user?.userHouse)!){
                result in
                self.latitude = result[0]
                self.longitude = result[1]
            }
        } else {
            self.streetTextField.isUserInteractionEnabled = true
            self.houseTextField.isUserInteractionEnabled = true
            self.appartmentTextField.isUserInteractionEnabled = true
        }
    }
    
    func applyLocale (){
        self.requiredLabel.text = "required".localized()
        self.useMyRegistrationLabel.text = "use_my_reg_date".localized()
        self.streetLabel.text = "street_required".localized()
        self.houseLabel.text = "house_required".localized()
        self.apartmentLabel.text = "apartment".localized()
        self.categoryRequiredLabel.text = "category_required".localized()
        self.subCategoryRequiredLabel.text = "subcategory_required".localized()
        self.useMyLocationLabel.text = "use_location".localized()
        self.mapLabel.text = "map".localized()
        self.descriptionLabel.text = "description_required".localized()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
