//
//  AddBidNowViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 06.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import M13Checkbox
import CoreLocation
import ActionSheetPicker_3_0
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import ImagePicker
import KCFloatingActionButton
import ImageViewer
import EGFormValidator
import Photos
import Toaster


extension UIImageView: DisplaceableView {}

class AddBidNowViewController: ValidatorViewController, AddBidNewView, UITextFieldDelegate,UITextViewDelegate, ImagePickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, GalleryItemsDatasource, GalleryDisplacedViewsDatasource {
   
    
    //MARK: - private data variables
    private var mCities = [City]()
    private var mStreets = [Street]()
    private var mCategories = [Category]()
    private var mSubCategories = [Category]()
    private var mCitiesName = [String]()
    private var mStreetsName = [String]()
    private var mCategoriesName = [String]()
    private var mSubCategoriesName = [String]()
    private var mUploadFileNames = [String]()
    private var mCitiesPickerIndex : Int = 0
    private var mStreetsPickerIndex : Int = 0
    private var mCategoriesPickerIndex : Int = 0
    private var mSubCategoriesPickerIndex : Int = 0
    private var latitude  : Double = 0.0
    private var longitude : Double = 0.0
    private var firstServiceCallComplete = false
    private var secondServiceCallComplete = false
    private var mUIImages = [UIImage]()
    
    //MARK: - views
    private var activityIndicatorView : NVActivityIndicatorView? = nil
    private var fab = KCFloatingActionButton()
    
    //MARK: - Outlets
    @IBOutlet weak var googleMapView: GMSMapView!

    @IBOutlet weak var grayView: UIView!
    @IBOutlet weak var checkBoxUseData: M13Checkbox!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var required: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var patronymicLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cityRequired: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var appartmentLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var subcategoryLabel: UILabel!
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var useMyDataRegistration: UILabel!
    @IBOutlet weak var useMyLocationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var houseLabel: UILabel!
    
    
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var subCategoryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var petronymicTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var houseTextField: UITextField!
    @IBOutlet weak var appartmentTextField: UITextField!
    
    @IBOutlet weak var errorLoginLabel: UILabel!
    @IBOutlet weak var errorEmailLabel: UILabel!
    @IBOutlet weak var errorCityLabel: UILabel!
    @IBOutlet weak var errorStreetLabel: UILabel!
    @IBOutlet weak var errorHouseLabel: UILabel!
    @IBOutlet weak var errorCategoryLabel: UILabel!
    @IBOutlet weak var errorSubcategoryLabel: UILabel!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!

    //MARK: - Other
    private var locationManager = CLLocationManager()
    private var mPresenter : AddBidNowPresenter? = nil

    
    //MARK: - LifeCycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeViews()
        self.startLoader()
        self.addValidators()
        
        
        mPresenter = AddBidNowPresenter(view: self)
        self.getInitialData()
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        imagesCollectionView.isHidden = true
        
    
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onValueChanged(_ sender: Any) {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }

    
    func getInitialData () {
        mPresenter?.getCities(){ (result) -> Void in
            self.mCities = result
            self.firstServiceCallComplete = true
            self.handleServiceCallCompletion()
        }
        mPresenter?.getCategories(){ (result)->Void in
            self.mCategories = result
            self.secondServiceCallComplete = true
            self.handleServiceCallCompletion()
        }
    }
    
    //MARK: - Presenter functions
   
    func getSubCategories (index : Int){
        self.mSubCategoriesName = [String]()
        
        mPresenter?.getSubCategories(index: index){ (result) -> Void in
            self.mSubCategories = result
            for subcategory in self.mSubCategories {

                self.mSubCategoriesName.append(subcategory.getCategoryName())
            }
        }
        
        
        
    }
   
    func getStreets(index : Int) {
        self.mStreetsName = [String]()
        mPresenter?.getStreets(index: index){ (result) -> Void in
            self.mStreets = result
            for  street in self.mStreets {
                self.mStreetsName.append(street.getStreetName())
            }
        }
        
    }
    
  
   
    
   
    
 
    //internal functions
    internal func initializeViews() {
       
        fab.addItem("Add photos".localized(), icon: UIImage(named: "add_photo")) { item in
            self.showImagePicker()
        }
        fab.addItem("Add bid", icon: UIImage(named: "add_bid")) { item in
            if self.validate() {
                self.mPresenter?.uploadPhotos(images: self.mUIImages){
                    (result) -> Void in
                    
                    var isChecked : Bool = false
                    
                    if self.checkBoxUseData._IBCheckState == "Checked"{
                        isChecked = true
                    } else {
                        isChecked = false
                    }
                    
                    self.mUploadFileNames = result
                    let bidData = AddBidAndReg(bidImages: self.mUploadFileNames, bidStreet: self.mStreets[self.mStreetsPickerIndex].getStreetID(), bidHouse: self.houseTextField.getValue() as! String, bidMessage: self.descriptionTextView.getValue() as! String, bidFlat: self.appartmentTextField.getValue() as! String, bidCategoriesID: self.mCategories[self.mCategoriesPickerIndex].getCategoryId(), bidSubcategoriesID: self.mSubCategories[self.mSubCategoriesPickerIndex].getCategoryId(), bitLatitude: self.latitude, bidLongitude: self.longitude, bidOnUserAddres: true, bidChangedManually: 1, bidCityID: self.mCities[self.mCitiesPickerIndex].getCityId(), userEmail: self.emailTextField.getValue() as! String, userName: self.nameTextField.getValue() as! String, userSurname: self.surnameTextField.getValue() as! String, userPatronymic: self.petronymicTextField.getValue() as! String, userStreet: self.mStreets[self.mStreetsPickerIndex].getStreetID(), userHouse: self.houseTextField.getValue() as! String, userFlat: self.appartmentTextField.getValue() as! String, userPhone: self.phoneTextField.getValue() as! String, isRegister: isChecked)
                    
                    self.mPresenter?.addBid(bidData: bidData){ (result) -> Void in
                        let isShowing = result as! Bool
                        if isShowing {
                            Toast(text: "bid_added".localized(), duration: 2.0).show()
                            var vc = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController")
                            if isChecked {
                                vc = self.storyboard?.instantiateViewController(withIdentifier: "BidsNavigationController")                             }
                            self.present(vc!, animated: true, completion: nil)
//                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                    }
                }
                
              
                
                //TODO: presenter make post request
            } else {
                self.scrollView.scrollRectToVisible((self.firstFailedControl?.frame)!, animated: true)
            }
        }
        self.view.addSubview(fab)
        //Others views
        descriptionTextView.delegate = self
        cityTextField.tag = 9
        streetTextField.tag = 10
        categoryTextField.tag = 11
        subCategoryTextField.tag = 12
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.layer.borderColor = UIColor.gray.cgColor
        let wigth = self.view.frame.width * 0.2
        let heigth = self.view.frame.height * 0.2
        let frame = CGRect(x: self.view.center.x - wigth/2.0 , y: self.view.center.y - heigth/2.0, width: wigth, height: heigth)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: .ballRotateChase, color: UIColor(red: 9/255.0, green: 148/255.0, blue: 122/255.0, alpha: 1))
        self.view.addSubview(activityIndicatorView!)
        
        }
    internal func moveCamera (lat : Double, lng : Double){
        googleMapView.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat,
                                              longitude: lng,
                                              zoom: 14)
        let marker =    GMSMarker(position: CLLocationCoordinate2D.init(latitude: lat, longitude: lng))
        
        marker.map = googleMapView
        googleMapView.animate(to: camera)
        
    }
    internal func startLoader() {
        if !(activityIndicatorView?.isAnimating)! {
        self.scrollView.isUserInteractionEnabled = false
        grayView.isHidden = false;
        activityIndicatorView?.startAnimating()
        }
        
    }
    internal func stopLoader() {
        if (activityIndicatorView?.isAnimating)!{
        grayView.isHidden = true;
        self.scrollView.isUserInteractionEnabled = true
        activityIndicatorView?.stopAnimating()
        }
        
    }
    

    //Delegate functions
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 9:
            self.view.endEditing(true)
            return  showDataPicker(textField: textField, title: "Choose city".localized(), data: self.mCitiesName);
        case 10:
            self.view.endEditing(true)
            return showDataPicker(textField: textField, title: "Choose street".localized(), data: self.mStreetsName)
        case 11:
            self.view.endEditing(true)
            return showDataPicker(textField: textField, title: "Choose category".localized(), data: self.mCategoriesName)
        case 12:
            self.view.endEditing(true)
            return showDataPicker(textField: textField, title: "Choose subcategory".localized(), data: self.mSubCategoriesName)
        default:
            return true
        }
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        print("didPress")
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count != 0 {
            imagesCollectionView.isHidden = false

        imagePicker.dismiss(animated: true, completion: nil)
            for imageAsset in imagePicker.stack.assets {
                let options = PHContentEditingInputRequestOptions()
                options.isNetworkAccessAllowed = true
                imageAsset.requestContentEditingInput(with: options) { (contentEditingInput: PHContentEditingInput?, _) -> Void in
                    let url = contentEditingInput!.fullSizeImageURL!
                    self.mUploadFileNames.append("\(url)")
                   
                }
            }
            
            
        for image in images {
            mUIImages.append(image)
        }
        self.imagesCollectionView.reloadData()
        }
        
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    private func textViewDidChange(textView: UITextView) {
        let textViewFixedWidth: CGFloat = self.descriptionTextView.frame.size.width
        let newSize: CGSize = self.descriptionTextView.sizeThatFits( CGSize(width: textViewFixedWidth, height: CGFloat(MAXFLOAT)))
        var newFrame: CGRect = self.descriptionTextView.frame
        var textViewYPosition = self.descriptionTextView.frame.origin.y
        var heightDifference = self.descriptionTextView.frame.height - newSize.height
        if (abs(heightDifference) > 20) {
            
            newFrame.size =   CGSize(width: fmax(newSize.width, textViewFixedWidth), height: newSize.height)
            newFrame.offsetBy(dx: 0.0, dy: 0)
        }
        self.descriptionTextView.frame = newFrame
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 1000), animated: true)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 800), animated: true)
    }
    
    
    //Other functions
    func addValidators (){
        self.addValidatorMandatory(toControl: emailTextField, errorPlaceholder: errorEmailLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: cityTextField, errorPlaceholder: errorCityLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: streetTextField, errorPlaceholder: errorStreetLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: houseTextField, errorPlaceholder: errorHouseLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: categoryTextField, errorPlaceholder: errorCategoryLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: subCategoryTextField, errorPlaceholder: errorSubcategoryLabel, errorMessage: "required_field".localized())
        self.addValidatorMandatory(toControl: descriptionTextView, errorPlaceholder: errorDescriptionLabel, errorMessage: "required_field".localized())
        
        self.addValidatorEmail(toControl: emailTextField, errorPlaceholder: errorEmailLabel, errorMessage: "Invalid email".localized())
        self.addValidatorMinLength(toControl: descriptionTextView, errorPlaceholder: errorDescriptionLabel, errorMessage: "Minimal length is 50".localized(), minLength: 50)
    }
    
    func handleServiceCallCompletion() {
        if firstServiceCallComplete && secondServiceCallComplete {
            self.stopLoader()
            for city in self.mCities {
                mCitiesName.append(city.getCityName())
            }
            for category in self.mCategories {
                mCategoriesName.append(category.getCategoryName())
            }

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.applyLocale()
    }
    
    //CollectionView functions
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mUIImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_view_cell_images", for: indexPath) as! ImagesCollectionViewCell
        cell.imageView.image = mUIImages[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        let leftInset : CGFloat
        let rightInset : CGFloat
        let collectionViewHeight = collectionView.bounds.height
        let collectionViewWidth = collectionView.bounds.width
        let numberOfItemsThatCanInCollectionView = Int(collectionViewWidth / collectionViewHeight)
        if numberOfItemsThatCanInCollectionView > mUIImages.count {
            let totalCellWidth = collectionViewHeight * CGFloat(mUIImages.count)
            let totalSpacingWidth: CGFloat = CGFloat(mUIImages.count) * (CGFloat(mUIImages.count) - 1)
            // leftInset, rightInset are the global variables which I am passing to the below function
            leftInset = (collectionViewWidth - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
            rightInset = -leftInset
        } else {
            leftInset = 0.0
            rightInset = -collectionViewHeight
        }
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showImagePreview(displacedIndex: indexPath.row)
    }
    
    
    //Image previewer functions
    func itemCount() -> Int {
        
        return mUIImages.count
    }
    
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        return UIImageView(image: mUIImages[index])
    }
    func provideGalleryItem(_ index: Int) -> GalleryItem {
            let image = mUIImages[index]
            return GalleryItem.image { $0(image) }
        
    }
    
    
    
    //Show elements functions
    internal func showImagePicker() {
        let imagePickerController = ImagePickerController()
        var config = Configuration()
        config.doneButtonTitle = "Finish".localized()
        config.recordLocation = false
        config.noImagesTitle = "Sorry! There are no images here!".localized()
        imagePickerController.delegate = self
        imagePickerController.configuration = config
        imagePickerController.imageLimit = 3 - mUIImages.count
        present(imagePickerController, animated: true, completion: nil)
    }
    internal func showDataPicker(textField: AnyObject, title: String, data: [String]) -> Bool{
        let textField = textField as! UITextField
        let actionPicker =  ActionSheetStringPicker(title: title, rows: data, initialSelection: 1, doneBlock: {
            picker, indexes, values in
            textField.text = values as? String
            switch textField.tag {
            case 9:
                self.mCitiesPickerIndex = indexes
                self.mPresenter?.getLocationByAddress(address: self.cityTextField.getValue() as! String){
                    result in
                    self.latitude = result[0]
                    self.longitude = result[1]
                }
                self.getStreets(index: indexes)
                break
            case 10:
                self.mStreetsPickerIndex = indexes
                self.mPresenter?.getLocationByAddress(address: (self.cityTextField.getValue() as! String) + (self.streetTextField.getValue() as! String)){
                    result in
                    self.latitude = result[0]
                    self.longitude = result[1]
                }
                break
            case 11:
                self.mCategoriesPickerIndex = indexes
                self.getSubCategories(index: indexes)
                break
            case 12:
                self.mSubCategoriesPickerIndex = indexes
                break
            default:
                break
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: textField)
        actionPicker?.setCancelButton(UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action:nil))
        actionPicker?.setDoneButton(UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action:nil))
        actionPicker?.show()
        return false
    }
    func showImagePreview (displacedIndex : Int) {
        
        let frame = CGRect(x: 0, y: 0, width: 200, height: 24)
        let headerView = CounterView(frame: frame, currentIndex: displacedIndex, count: mUIImages.count)
        let footerView = CounterView(frame: frame, currentIndex: displacedIndex, count: mUIImages.count)
        let galleryViewController = GalleryViewController(startIndex: displacedIndex, itemsDatasource: self, displacedViewsDatasource: self, configuration: self.galleryConfiguration())
        galleryViewController.headerView = headerView
        galleryViewController.footerView = nil
        
        
        galleryViewController.landedPageAtIndexCompletion = { index in
            headerView.currentIndex = index
            footerView.currentIndex = index
        }
        
        self.presentImageGallery(galleryViewController)

    }
    
    func showToast(message: String) {
        Toast(text: message, duration: 2).show()
    }
    
    func galleryConfiguration() -> GalleryConfiguration {
        return [
            
            GalleryConfigurationItem.pagingMode(.carousel),
            GalleryConfigurationItem.presentationStyle(.displacement),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            
            GalleryConfigurationItem.maximumZoolScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.linear),
            
            GalleryConfigurationItem.statusBarHidden(true),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(false),
            GalleryConfigurationItem.displacementInsetMargin(50)
        ]
    }
    func applyLocale (){
        self.loginLabel.text = "Login".localized()
        self.emailLabel.text = "email_required".localized()
        self.surnameLabel.text = "surname".localized()
        self.nameLabel.text = "name".localized()
        self.patronymicLabel.text = "patronymic".localized()
        self.phoneLabel.text = "phone".localized()
        self.useMyDataRegistration.text = "use_my_data".localized()
        self.cityRequired.text = "city_required".localized()
        self.streetLabel.text = "street_required".localized()
        self.houseLabel.text = "house_required".localized()
        self.appartmentLabel.text = "apartment".localized()
        self.categoryLabel.text = "category_required".localized()
        self.subcategoryLabel.text = "subcategory_required".localized()
        self.mapLabel.text = "map".localized()
        self.useMyLocationLabel.text = "use_location".localized()
        self.descriptionLabel.text = "description_required".localized()
    }
    


    
    
}
