//
//  AddBidNowPresenter.swift
//  cityonline
//
//  Created by Michael Ovsienko on 07.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
import Alamofire
class AddBidNowPresenter : AddBidNowPresenterContract {
    
    private var cities = [City]()
    private var streets = [Street]()
    private var categories = [Category]()
    private var subCategories = [Category]()
    private var images = [String]()

    
    private var view : AddBidNewView? = nil
    
    
    
    required init(view : AddBidNewView) {
        self.view = view
        
    }
    func getCities(completion: @escaping (_ result: [City])->Void  ) {
        view?.startLoader()
        Alamofire.request(Constants.GET_CITIES_API, method: .get, parameters: nil).responseJSON{
            response in
            switch response.result {
                
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                guard let citiesNSArray  = jsonResponse?["cities"] as? NSArray,
                    let bufCities = City.modelsFromDictionaryArray(array: citiesNSArray) as? [City] else {
                        return
                }
                self.cities = bufCities
                completion(self.cities)
                break
            case .failure(let error):
                print(error)
                self.view?.stopLoader()
                break
            }
            
        }
    }
    func getStreets(index: Int, completion: @escaping (_ result: [Street])->Void)  {
        view?.startLoader()
        Alamofire.request(Constants.GET_STREETS_API+String(cities[index].getCityId()), method: .get, parameters: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                
                guard let streetsNSArray = jsonResponse?["streets"] as? NSArray,
                    let bufStreets = Street.modelsFromDictionaryArray(array: streetsNSArray) as? [Street] else {
                        return
                }
                self.streets = bufStreets
                self.view?.stopLoader()
                completion(self.streets)
                break
            case .failure(let error):
                self.view?.stopLoader()
                print(error)
                break
            }
            
        }
        
    }
    
    func getStreetsByCityID(cityID: Int, completion: @escaping (_ result: [Street])->Void)  {
        view?.startLoader()
        Alamofire.request(Constants.GET_STREETS_API+String(cityID), method: .get, parameters: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                
                guard let streetsNSArray = jsonResponse?["streets"] as? NSArray,
                    let bufStreets = Street.modelsFromDictionaryArray(array: streetsNSArray) as? [Street] else {
                        return
                }
                self.streets = bufStreets
                self.view?.stopLoader()
                completion(self.streets)
                break
            case .failure(let error):
                self.view?.stopLoader()
                print(error)
                break
            }
            
        }
        
    }
    
    func getCategories(completion: @escaping (_ result: [Category])->Void  )  {
        view?.startLoader()
        let parameters : [String:Int] = ["city" : 616]
        Alamofire.request(Constants.GET_CATEGORIES_API, method: .get, parameters: parameters).responseJSON{
            response in
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                
                guard let categoriesNSArray  = jsonResponse?["categories"] as? NSArray,
                    let bufCategories = Category.modelsFromDictionaryArray(array: categoriesNSArray) as? [Category] else {
                        return
                }
                self.categories = bufCategories
                completion(self.categories)
                break
            case .failure(let error):
                print(error)
                self.view?.stopLoader()

                break
            }
            
        }

    }
    
    func uploadPhotos (images : [UIImage], completion: @escaping (_ result: [String]) -> Void){
        self.view?.startLoader()
        Alamofire.upload(multipartFormData: { multipartFormData in
            var index = 1
            for image in images {
                let imageData: Data = (UIImageJPEGRepresentation(image, 0.5) as Data?)!
                
                multipartFormData.append(imageData, withName: "images", fileName: "home-\(index)", mimeType: "image/jpeg")
                
                index += 1
            }
        }, to: Constants.POST_UPLOAD_IMAGES, encodingCompletion: { result in
            self.view?.stopLoader()
            switch result {
            case .success(let upload, _, _):
                //TODO get images names
                upload.responseJSON { response in
                    let jsonResponse = response.result.value as? [AnyObject]
                    for item in  jsonResponse! {
                        let name = (item["name"] as! String)
                        self.images.append(name)
                    }
                    completion(self.images)
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                completion(self.images)
            }
        })
    }
    
    func addBid (bidData  : AddBidAndReg, completion: @escaping (_ result: Bool)->Void) {
        view?.startLoader()
        let parameters = bidData.getParameters()
        print(parameters)
        Alamofire.request(Constants.POST_ADD_BID, method: .post, parameters: parameters).responseJSON {
            response in
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                let statusCode = jsonResponse?["status_code"] as? Int
                if statusCode != 2002 {
                    self.view?.showToast(message: Utils.checkStatusCode(statusCode: statusCode!))
                    completion(false)

                } else {
                        let payload = jsonResponse?["payload"] as! NSDictionary
                        let token = payload["token"] as! String
                        let userData = payload["user"] as! NSDictionary
                        let flat : String
                        if let buffer = userData["flat"] as? String {
                            flat = buffer
                        } else {
                            flat = ""
                        }
                        let user = User(city: userData["cityID"] as! Int, street: userData["street"] as! Int , flat: flat, house: userData["house"] as! String)
                        
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
                        UserDefaults.standard.set(encodedData,forKey: Constants.USER_DATA)
                        UserDefaults.standard.set(token, forKey: Constants.TOKEN)
                    Singleton.sharedInstance.token = token
                        
                    completion(true)

                }
                self.view?.stopLoader()
                break
            case .failure(let error):
                print(error)
                self.view?.stopLoader()
                completion(false)
                break
            }
            
        }
    }
    func addBidWithToken (bidData  : AddBidAndReg, token : String,completion: @escaping (_ result: Bool)->Void) {
        view?.startLoader()
        let parameters = bidData.getParametersWithToken()
        let header : [String:String] = [
            "authorization" : token
        ]
        print(parameters)
        Alamofire.request(Constants.POST_ADD_BID, method: .post, parameters: parameters, headers : header).responseJSON {
            response in
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                let statusCode = jsonResponse?["status_code"] as? Int
                if statusCode != 2004 {
                    self.view?.showToast(message: Utils.checkStatusCode(statusCode: statusCode!))
                    completion(false)
                    
                } else {
                    completion(true)
                }
                self.view?.stopLoader()
                break
            case .failure(let error):
                print(error)
                self.view?.stopLoader()
                completion(false)
                break
            }
            
        }
    }
    
    func getSubCategories(index: Int, completion: @escaping(_ result: [Category])->Void) {
        view?.startLoader()
        print(Constants.GET_SUBCATEGORIES_API)
        let parameters : [String:Int] = ["city" : 616]
        Alamofire.request(Constants.GET_SUBCATEGORIES_API + String(self.categories[index].getCategoryId()), method: .get, parameters: parameters).responseJSON{
            response in
            self.view?.stopLoader()
            switch response.result {
            case .success:
                let jsonResponse  = response.result.value as?  NSDictionary
                
                guard let subCategoriesNSArray  = jsonResponse?["subcategories"] as? NSArray,
                    let bufCategories = Category.modelsFromDictionaryArray(array: subCategoriesNSArray) as? [Category] else {
                        return
                }
                self.subCategories = bufCategories
                self.view?.stopLoader()
                completion(self.subCategories)
                break
            case .failure(let error):
                print(error)
                self.view?.stopLoader()
                break
            }
            
        }


    }
   
    func getLocationByAddress(address: String, completion: @escaping(_ result: [Double])->Void)  {
        let parameters : [String:String] = ["address" : address,
                                            "key":"AIzaSyBElD626SwQH86QKhem9j9qKpMBaqkJr7g"]
        Alamofire.request(Constants.GET_LOCATION_API, method: .get, parameters: parameters).responseJSON {
            response in
            switch response.result {
            case .success:
                guard  let jsonResponse = response.result.value as? NSDictionary,
                    let results = jsonResponse["results"] as? NSArray else {
                        return
                }
                if (results.count != 0){
                    guard let res = results[0] as? [String:AnyObject],
                    let geometry = res["geometry"] as? [String:AnyObject],
                    let location = geometry["location"] as? [String:AnyObject],
                    let lat = location["lat"] as? Double,
                        let lng = location["lng"] as? Double else {
                            return
                    }
                    let latLng  = [lat,lng]
                    completion(latLng)
                    self.view?.moveCamera(lat: lat, lng: lng)
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    
    
}
