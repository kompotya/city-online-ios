//
//  Singleton.swift
//  cityonline
//
//  Created by Michael Ovsienko on 21.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Singleton {
    var user : User? = nil
    var token : String = ""
    static let sharedInstance = Singleton()
    private init(){}
}
