//
//  BidsViewController.swift
//  cityonline
//
//  Created by Michael Ovsienko on 18.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import SDWebImage
import KCFloatingActionButton


class BidsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, BidsView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bidsCollectionView: UICollectionView!
    private var mPresenter : BidsPresenter? = nil
    private var mBids = [Bid]()
    private var token : String = ""
    private var floatingButton = KCFloatingActionButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.mPresenter = BidsPresenter(view: self)
        self.token = UserDefaults.standard.string(forKey: Constants.TOKEN)!
        Singleton.sharedInstance.token = token
        self.mPresenter?.getUsersBids(token: token){
            result in
            self.mBids = result
            self.reloadData()
        }
        self.initFabButton()
        
        if UserDefaults.standard.data(forKey: Constants.USER_DATA) != nil  {
            let user = UserDefaults.standard.data(forKey: Constants.USER_DATA)
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: user!) as! User
            Singleton.sharedInstance.user = decodedData
        }
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mBids.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_bid", for: indexPath) as! BidsCollectionViewCell
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.bidImage.layer.cornerRadius = cell.bidImage.frame.size.width / 2;
        cell.bidImage.clipsToBounds = true;
        
        cell.bidAddress.text = mBids[indexPath.row].bidStreetAddress + " " + mBids[indexPath.row].bidHouseAddress
        cell.bidNumber.text = mBids[indexPath.row].bidId
        cell.bidCategory.text = mBids[indexPath.row].bidCategory
        cell.bidDate.text = mBids[indexPath.row].bidDateInsert
        cell.bidStatus.text = mBids[indexPath.row].bidStatus
        cell.bidImage.setShowActivityIndicator(true)
        cell.bidImage.setIndicatorStyle(.gray)
        if mBids[indexPath.row].bidPhotos.count != 0{
            cell.bidImage.sd_setImage(with: URL(string: self.mBids[indexPath.row].bidPhotos[0]))
        } else {
            cell.bidImage.image = UIImage(named: "icon_logo-web")
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var vc : DetailBidViewController = self.storyboard!.instantiateViewController(withIdentifier: "DetailBidViewController") as! DetailBidViewController
        vc.bidDetail = self.mBids[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "my_requests".localized()
        self.navigationItem.backBarButtonItem?.title = "back".localized()

    }
    
    func initFabButton (){
        floatingButton.buttonImage = UIImage(named: "arrow-up")
        floatingButton.addItem("Add bid".localized(), icon: UIImage(named: "add")) { item in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewBidViewController")
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        floatingButton.addItem("Logout".localized(), icon: UIImage(named: "logout")) { item in
            UserDefaults.standard.setValue(nil, forKey: Constants.TOKEN)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        floatingButton.addItem("Settings".localized(), icon: UIImage(named: "settings")) { item in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController")
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        self.view.addSubview(floatingButton)
    }
    
    func stopLoader() {
        print("stop")
        if activityIndicator.isAnimating {
           activityIndicator.stopAnimating()
        }
    }
    func startLoader() {
        print("stop")
        if !activityIndicator.isAnimating {
            activityIndicator.startAnimating()
        }
    }
    func reloadData() {
        self.bidsCollectionView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
